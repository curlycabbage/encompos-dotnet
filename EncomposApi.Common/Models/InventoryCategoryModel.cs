﻿
namespace EncomposApi.Models
{
    public class InventoryCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
