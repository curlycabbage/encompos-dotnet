﻿
namespace EncomposApi.Models
{
    public class BottleDepositModel
    {
        public decimal Qty { get; set; }
        public decimal UnitFee { get; set; }
        public decimal TotalFee { get; set; }
    }
}
