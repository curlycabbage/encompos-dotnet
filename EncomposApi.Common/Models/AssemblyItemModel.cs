﻿
namespace EncomposApi.Models
{
    public class AssemblyItemModel
    {
        public string ProductCode { get; set; }
        public string AssemblyCode { get; set; }
        public int SellingQty { get; set; }
        public decimal SellingPrice { get; set; }
    }
}
