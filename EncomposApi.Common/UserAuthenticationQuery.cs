﻿namespace EncomposApi
{
    public record UserAuthenticationQuery
    {
        public string PIN { get; init; }
    }
}
