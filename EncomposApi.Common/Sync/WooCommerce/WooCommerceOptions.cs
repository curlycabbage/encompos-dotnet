﻿namespace EncomposApi.Sync.WooCommerce
{
    public class WooCommerceOptions
    {
        public string BaseAddress { get; set; }
        public string Key { get; set; }
        public string Secret { get; set; }
    }
}
