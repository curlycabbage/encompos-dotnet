﻿namespace EncomposApi.Enums
{
    public enum InventoryType
    {
        Standard = 1,
        Service = 2,
        Serialized = 3,
        Assembly = 4,
        Fee = 5,
        FixedTax = 6,
        PureRental = 7,
        ManufacturerSellThru = 8,
        Apparel = 9,
        VarietyPack = 10,
        FlowThru = 11,
        WorkOrderService = 12,
        WebParent = 13,
        Voucher = 14,
        OptionItem = 15
    }
}
