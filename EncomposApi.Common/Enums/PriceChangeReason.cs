﻿namespace EncomposApi.Enums
{
    public enum PriceChangeReason
    {
        ManualFormAdjustment = 0,
        Tactical = 1,
        IncreaseProfits = 2,
        IncreaseTurnover = 3,
        SupplierCostChange = 4,
        PriceCorrection = 5,
        SpecialPricing = 6,
        PriceRounding = 7,
        ApplySuggestedRetail = 8,
        DepartmentRelocation = 9,
        ListEditPriceAdjustment = 10,
        HqAdjustment = 11,
        NewItem = 12,
        CatalogUpdate = 13,
        CompetitorPriceMatch = 14,
        HandheldPriceAdjustment = 15,
        MassPriceAdjustment = 16,
    }
}
