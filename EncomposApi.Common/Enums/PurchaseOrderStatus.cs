﻿namespace EncomposApi.Enums
{
    public enum PurchaseOrderStatus
    {
        InProgress = 0,
        Complete = 1
    }
}
