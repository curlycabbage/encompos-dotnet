﻿namespace EncomposApi.Enums
{
    public enum TagRequestReason
    {
        FormEdit = 1,
        PoCostChange = 2,
        RePrint = 3
    }
}
