﻿namespace EncomposApi.Enums
{
    public enum AssemblyType
    {
        KitProduct = 0,
        LinkedSeller = 1,
        FusionItem = 2,
    }
}
