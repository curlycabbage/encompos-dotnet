﻿namespace EncomposApi.Enums
{
    public enum CustomerAccountType
    {
        Personal = 0,
        Business = 1
    }
}
