﻿namespace EncomposApi
{
    public record UserQuery
    {
        public string PIN { get; init; }
    }
}
