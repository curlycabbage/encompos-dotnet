﻿namespace EncomposApi
{
    public record CustomerQuery
    {
        public string[] Emails { get; init; }
    }
}
